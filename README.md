# Example of publishing images using fgallery

This project shows how to publish photos
using
[fgallery](http://www.thregr.org/~wavexx/software/fgallery/index.html).
It essentially
publishing
[plain html](https://gitlab.com/pages/plain-html/tree/master).  (Note,
this has nothing to do with darktable, but I'd obviously export the
photos to the "photos" directory using Darktable).

See the result [here](https://tproulund.gitlab.io/dolomites_2022).
